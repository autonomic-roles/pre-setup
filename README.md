## Pre-Setup
This role will run some tasks to setup a Debian based host so that it can run other Ansible playbooks on it.

## Install aptitude
Three of the ansible apt module  upgrade modes (full, safe and its alias yes) required aptitude up to 2.3, since 2.4 apt-get is used as a fall-back.

Source https://docs.ansible.com/ansible/latest/modules/apt_module.html

We can use the ```force_apt_get``` parameter so that apt-get will be used to install aptitude.
